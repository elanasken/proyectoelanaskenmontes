<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitda3e661d9586580b8a45ef33bb05e173
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Twig\\' => 5,
        ),
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Twig\\' => 
        array (
            0 => __DIR__ . '/..' . '/twig/twig/src',
        ),
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Twig_' => 
            array (
                0 => __DIR__ . '/..' . '/twig/twig/lib',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitda3e661d9586580b8a45ef33bb05e173::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitda3e661d9586580b8a45ef33bb05e173::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitda3e661d9586580b8a45ef33bb05e173::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
