<?php  
//Fichero controllers/blogController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/postModel.php');
require('models/blogModel.php');
$blog=new Blog();

//Recogemos la accion que queremos realizar con isset($_GET['accion'])
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
} // Fin del if(isset($_GET['accion']))

switch($accion){
	case 'ver':
		$entrada=$blog->dimeEntrada($_GET['id']);
		echo $twig->render('entrada.html.twig', Array('entrada'=>$entrada));
		break;

	case 'insertar':
		echo $twig->render('entradaFormulario.html.twig', Array('accion'=>'insercion'));
		break;

	case 'insercion':
		$titulo=$_POST['titulo'];
		$contenido=$_POST['contenido'];
		$autor=$_POST['autor'];
		$fecha=date('Y-m-d h:i:s');
		$blog->nuevaEntrada($titulo, $contenido, $autor, $fecha);
		header('location: index.php?c=blogController.php');
		break;

	case 'borrar':
		$id=$_GET['id'];
		$blog->borrarEntrada($id);
		header('location: index.php?c=blogController.php');
		break;

	case 'modificar':
		$entrada=$blog->dimeEntrada($_GET['id']);
		echo $twig->render('entradaFormulario.html.twig', Array('entrada'=>$entrada, 'accion'=>'modificacion'));
		break;

	case 'modificacion':
	 	$titulo=$_POST['titulo'];
	 	$contenido=$_POST['contenido'];
	 	$id=$_POST['id'];
	 	$autor=$_POST['autor'];
	 	$blog->guardarEntrada($id, $titulo, $contenido, $autor);
	 	header('location: index.php?c=blogController.php');
	 	break;

	case 'listado':
	default:
		$entradas=$blog->dimeEntradas();
		echo $twig->render('entradas.html.twig', Array('entradas'=>$entradas));
		break;
} //Fin del switch($accion)


?>