<?php  
//Fichero controllers/categoriasController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/reviewModel.php');
require('models/reviewsModel.php');
$reviews=new Reviews();

//Recogemos la accion que queremos realizar con isset($_GET['accion'])
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
} // Fin del if(isset($_GET['accion']))

switch($accion){
	case 'ver':
		$review=$reviews->dimeElemento($_GET['id']);
		echo $twig->render('review.html.twig', Array('review'=>$review));
		break;

	case 'insertar':
		echo $twig->render('reviewFormulario.html.twig', Array('accion'=>'insercion'));
		break;

	case 'insercion':
		$nombre=$_POST['nombre'];
		$descripcion=$_POST['descripcion'];
		$autor=$_POST['autor'];
		$fecha=date('Y-m-d h:i:s');
		$reviews->nuevoElemento($nombre, $descripcion, $autor, $fecha);
		header('location: index.php?c=reviewsController.php');
		break;

	case 'borrar':
		$id=$_GET['id'];
		$reviews->borrarElemento($id);
		header('location: index.php?c=reviewsController.php');
		break;

	case 'modificar':
		$review=$reviews->dimeElemento($_GET['id']);
		echo $twig->render('reviewFormulario.html.twig', Array('review'=>$review, 'accion'=>'modificacion'));
		break;

	case 'modificacion':
	 	$nombre=$_POST['nombre'];
	 	$descripcion=$_POST['descripcion'];
	 	$id=$_POST['id'];
	 	$autor=$_POST['autor'];
	 	$reviews->guardarElemento($id, $nombre, $descripcion, $autor);
	 	header('location: index.php?c=reviewsController.php');
	 	break;

	case 'listado':
	default:
		$reviews=$reviews->dimeElementos();
		echo $twig->render('reviews.html.twig', Array('reviews'=>$reviews));
		break;
} //Fin del switch($accion)
?>