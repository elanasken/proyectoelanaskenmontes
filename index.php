<?php  
//En la primera linea:
session_start();

//Llamamos al archivo de conexion a bbdd
require('includes/conexion.php');

//llamamos a las librerias que hay en /vendor/
require('vendor/autoload.php');

//Debajo del resto de require
require('includes/login.php');

//Le decimos donde van a ir las plantillas de twig
$loader = new Twig_Loader_Filesystem('views/');

//ESTO PARA DESARROLLO
$twig = new Twig_Environment($loader);
//ESTO PARA PRODUCCION
//$twig = new Twig_Environment($loader, array('cache' => 'cache/'));

//Debajo de $twig = new Twig_Environment($loader);
$twig->addGlobal('session', $_SESSION);

//Vamos a pensar, a ver, a que controlador queremos llamar
//Por defecto, llamamos a blogController.php
if(isset($_GET['c'])){
  $c=$_GET['c'];
}else{
  $c='blogController.php';
}

//Llamo al controlador
require('controllers/'.$c);

//Desconectas
$conexion->close();
?>