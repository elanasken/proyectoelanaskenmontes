<?php  
//Fichero models/categoriasModel.php

class Reviews{

	public $elementos; //Sera un VECTOR de Productos

	public function __construct(){
		$this->elementos=[]; //Le digo que va a ser un VECTOR vacio
	}

	public function dimeElementos(){
		global $conexion;
		$sql="SELECT * FROM reviews ORDER BY nombre ASC";
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->elementos[]=new Review($registro);
		}
		return $this->elementos;
	}

	public function dimeElemento($id){
		global $conexion;
		$sql="SELECT * FROM reviews WHERE id=$id";
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		$elemento=new Review($registro);
		return $elemento;
	}

	public function nuevoElemento($nombre, $descripcion, $autor, $fecha){
		global $conexion;
		$sql="INSERT INTO reviews(nombre, descripcion, autor, fecha)VALUES('$nombre', '$descripcion', '$autor', '$fecha')";
		$consulta=$conexion->query($sql);
	}

	public function borrarElemento($id){
		global $conexion;
		$sql="DELETE FROM reviews WHERE id=$id";
		$consulta=$conexion->query($sql);
	}

	public function guardarElemento($id, $nombre, $descripcion, $autor){
		global $conexion;
		$sql="UPDATE reviews SET nombre='$nombre', descripcion='$descripcion', autor='$autor' WHERE id=$id";
		$consulta=$conexion->query($sql);
	}
	

} //Fin de la class Almacen
?>