<?php  
//Fichero models/blogModel.php

class Blog{

	public $entradas; //Sera un VECTOR de Posts

	public function __construct(){
		$this->entradas=[]; //Le digo que va a ser un VECTOR vacio
	}

	public function dimeEntradas(){
		global $conexion; //Hago alusion a la conexion GLOBAL
		$sql="SELECT * FROM blog ORDER BY fecha DESC";
		//$consulta=$GLOBALS['conexion']->query($sql); //POR PROBAR
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->entradas[]=new Post($registro);
		}
		return $this->entradas; //Devuelvo un ARRAY de POSTS
	}

	public function dimeEntrada($id){
		global $conexion; //Hago alusion a la conexion GLOBAL
		$sql="SELECT * FROM blog WHERE id=$id";
		//$consulta=$GLOBALS['conexion']->query($sql); //POR PROBAR
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		$entrada=new Post($registro);
		return $entrada; //Devuelvo un solo POST
	}

	public function nuevaEntrada($titulo, $contenido, $autor, $fecha){
		global $conexion;
		$sql="INSERT INTO blog(titulo, contenido, autor, fecha)VALUES('$titulo', '$contenido', '$autor', '$fecha')";
		$consulta=$conexion->query($sql);
	}

	public function borrarEntrada($id){
		global $conexion;
		$sql="DELETE FROM blog WHERE id=$id";
		$consulta=$conexion->query($sql);
	}

	public function guardarEntrada($id, $titulo, $contenido, $autor){
		global $conexion;
		$sql="UPDATE blog SET titulo='$titulo', contenido='$contenido', autor='$autor' WHERE id=$id";
		$consulta=$conexion->query($sql);
	}

} //Fin de la class Blog
?>